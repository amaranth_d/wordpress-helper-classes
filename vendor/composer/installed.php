<?php return array(
    'root' => array(
        'name' => 'denispatriev/wordpress-helpers',
        'pretty_version' => 'dev-master',
        'version' => 'dev-master',
        'reference' => '410e78d6c715df9aa7eda4f21a0fdb248d2e6211',
        'type' => 'library',
        'install_path' => __DIR__ . '/../../',
        'aliases' => array(),
        'dev' => true,
    ),
    'versions' => array(
        'denispatriev/wordpress-helpers' => array(
            'pretty_version' => 'dev-master',
            'version' => 'dev-master',
            'reference' => '410e78d6c715df9aa7eda4f21a0fdb248d2e6211',
            'type' => 'library',
            'install_path' => __DIR__ . '/../../',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
    ),
);
