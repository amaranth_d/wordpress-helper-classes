<?php

namespace Denispatriev\WordpressHelpers\Database;

use WP_Post;

/**
 *
 */
class WP_Repository {

    /**
     * @var string
     */
    protected string $post_type = 'post';

    /**
     * @var null
     */
    public static $instance = null;

    /**
     * Array with valid post_status arguments.
     */
    protected const VALID_STATUS = [
        'publish',
        'draft',
        'private',
        'trash'
    ];

    /**
     * Class constructor.
     */
    public function __construct()
    {
        if(!defined('ABSPATH')) {
            throw new \Error('WordPressHelpers: package was started out of WordPress environment!');
        }
    }

    /**
     * @return static|null
     */
    public static function get_instance()
    {
        if(!self::$instance) {
            self::$instance = new static();
        }

        return self::$instance;
    }

    /**
     * @param string $post_status
     * @return void
     */
    protected function check_valid_status(string $post_status): void
    {
        $valid = in_array($post_status, self::VALID_STATUS);

        if(!$valid) {
            throw new \Error('WordPressHelpers: Post Status argument is invalid!');
        }
    }

    /**
     * @param int $limit
     * @param string $post_status
     * @param array $additional_args
     * @return array
     */
    public function get(int $limit = 15, string $post_status = 'publish', array $additional_args = []): array
    {
        $this->check_valid_status($post_status);

        $args = [
            'post_type' => $this->post_type,
            'numberposts' => $limit,
            'post_status' => $post_status,
        ];

        if($additional_args) {
            $args = array_merge_recursive($args, $additional_args);
        }

        return get_posts($args);
    }

    /**
     * @param int $posts_per_page
     * @param int $page
     * @param string $post_status
     * @return array
     */
    public function get_by_page(int $posts_per_page = 15, int $page = 1, string $post_status = 'publish'): array
    {
        $this->check_valid_status($post_status);

        $args = [
            'post_type' => $this->post_type,
            'posts_per_page' => $posts_per_page,
            'post_status' => $post_status,
            'paged' => $page
        ];

        return get_posts($args);
    }

    /**
     * @return int[]|WP_Post[]
     */
    public function get_all_private_posts(int $count = 15, string $post_status = 'private'): array
    {
        $this->check_valid_status($post_status);

        return get_posts([
            'numberposts' => $count,
            'post_status' => $post_status,
            'post_type' => $this->post_type,
        ]);
    }
}
